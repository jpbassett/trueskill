/*
 * Gaussian.cpp
 *
 *  Created on: May 27, 2018
 *      Author: jesse
 */

#include "Gaussian.h"
#include <math.h>


Gaussian::Gaussian() :
		m_mu(0.0), m_variance(0.0), m_sigma(0.0)
{

}

Gaussian::Gaussian(const double mu, const double variance) :
		m_mu(mu), m_variance(variance)
{
	this->m_sigma = sqrt(variance);
}

Gaussian::~Gaussian()
{
	// TODO Auto-generated destructor stub
}

void Gaussian::set_sigma(const double sigma)
{
	this->m_sigma = sigma;
	this->m_variance = pow(sigma, 2);
}

void Gaussian::set_variance(const double variance)
{
	this->m_variance = variance;
	this->m_sigma = sqrt(variance);
}
