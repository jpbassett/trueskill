/*
 * testelo.cpp
 *
 *  Created on: May 28, 2018
 *      Author: jesse
 */

#include <elo.h>
#include <gtest/gtest.h>

TEST(EloTest, SingleTest)
{
	EloEnv env = DEFAULT_ENV;

}

/*
 * Suppose Player A has a rating of 1613, and plays in a five-round tournament.
 * He or she:
 * 	loses to a player rated 1609,
 * 	draws with a player rated 1477,
 * 	defeats a player rated 1388,
 * 	defeats a player rated 1586,
 * 	and loses to a player rated 1720.
 * The player's actual score is (0 + 0.5 + 1 + 1 + 0) = 2.5.
 * The expected score, calculated according to the formula above, was (0.51 + 0.69 + 0.79 + 0.54 + 0.35) = 2.88.
 * Therefore, the player's new rating is (1613 + 32(2.5 − 2.88)) = 1601, assuming that a K-factor of 32 is used.
 */
TEST(EloTest, BatchTest)
{
	std::vector<EloRating> ratings, results;
	std::vector<BatchResult> outcomes;
	EloEnv env = DEFAULT_ENV;

	env.experienced_k = 32.0;

	ratings.resize(6);
	outcomes.resize(5);

	rating_t rvalues[6] = {1613, 1609, 1477, 1388, 1586, 1720};
	outcome_t ovalues[5] = {0, 0.5, 1, 1, 0};

	int idx;
	for(idx = 0; idx < 6; idx++)
	{
		ratings[idx].rating = rvalues[idx];
		ratings[idx].experience = 100;
		if(idx > 0)
		{
			outcomes[idx - 1].p1_idx = 0;
			outcomes[idx - 1].p2_idx = idx;
			outcomes[idx - 1].result = ovalues[idx - 1];
		}
	}

	rate_batch(env, ratings, outcomes, results);

	ASSERT_NEAR(results[0].rating, 1601, 1);
}

int main(int argc, char* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
