/*
 * elo.c
 *
 *  Created on: May 28, 2018
 *      Author: jesse
 */

#include "elo.h"
#include <math.h>
#include <map>

/**
 * \brief Q function
 *
 * \param rating The ELO rating of the player
 *
 * \return The scaled Q value for the player
 */
#define ELO_Q(rating) pow(10, rating / 400)

/**
 * \brief Calculate the expected outcome of a match between two player ratings
 *
 * \param a ELO Rating for player A
 * \param b ELO Rating for player B
 *
 * \return Expected outcome for player A of this matchup
 */
outcome_t expected(const rating_t a, const rating_t b)
{
	return ELO_Q(a) / (ELO_Q(a) + ELO_Q(b));
}

/**
 * \brief Calculate the K value for updating a rating given an ELO environment
 *
 * \param env The ELO environment to use
 * \param experience The number of games played by this player
 *
 * \return The K value to scale the rating update by
 */
double k_value(const struct EloEnv& env, const unsigned int experience)
{
	if (experience > env.provisional_games)
	{
		return env.experienced_k;
	}
	else
	{
		if (env.gradiated_k)
		{
			return (env.provisional_games - experience)
					* ((env.provisional_k - env.experienced_k)
							/ env.provisional_games);
		}
		else
		{
			return env.provisional_k;
		}
	}
}

rating_t calculate_new_rating(const struct EloEnv& env, const rating_t prior,
		const unsigned int experience, const outcome_t expected,
		const outcome_t performace)
{
	return prior + k_value(env, experience) * (performace - expected);
}

void rate_1v1(const struct EloEnv& env, const struct EloRating& p1,
		const struct EloRating& p2, const outcome_t outcome,
		struct EloRating& rslt_p1, struct EloRating& rslt_p2)
{
	outcome_t exp_a;

	exp_a = expected(p1.rating, p2.rating);

	rslt_p1.rating = calculate_new_rating(env, p1.rating, p1.experience, exp_a,
			outcome);
	rslt_p1.experience = p1.experience + 1;

	rslt_p2.rating = calculate_new_rating(env, p2.rating, p2.experience,
			1 - exp_a, 1 - outcome);
	rslt_p2.experience = p2.experience + 1;
}

/**
 * \brief Interim structure to hold batch results
 */
struct BatchInterim
{
	double exp;
	double perf;
	unsigned int games;
};

void rate_batch(const struct EloEnv& env, const std::vector<EloRating>& players,
		const std::vector<BatchResult>& outcomes,
		std::vector<EloRating>& results)
{
	unsigned int idx;
	outcome_t exp;

	std::vector<BatchInterim> tally;
	tally.resize(players.size());

	std::vector<BatchResult>::const_iterator it;

	// Tally up the outcomes by player
	for (it = outcomes.begin(); it != outcomes.end(); ++it)
	{
		exp = expected(players[it->p1_idx].rating, players[it->p2_idx].rating);
		tally[it->p1_idx].exp += exp;
		tally[it->p1_idx].perf += it->result;
		tally[it->p1_idx].games++;

		tally[it->p2_idx].exp += 1 - exp;
		tally[it->p2_idx].perf += 1 - it->result;
		tally[it->p2_idx].games++;
	}

	// Calculate the new ratings for all players
	results.resize(players.size());

	for (idx = 0; idx < players.size(); idx++)
	{
		results[idx].rating = calculate_new_rating(env, players[idx].rating,
				players[idx].experience, tally[idx].exp, tally[idx].perf);
		results[idx].experience = players[idx].experience + tally[idx].games;
	}
}
