/*
 * trueskill.cpp
 *
 *  Created on: Feb 14, 2018
 *      Author: jesse
 */

#include "trueskillutil.h"
#include <cmath>
#include <trueskill.hpp>

typedef std::vector<Performance> perfgroup_t;
typedef std::vector<perfgroup_t> matchprecisions_t;

void skill_to_performance(const Skill& r, Performance& p)
{
	p.set_sigma(1 / (r.mu() * r.variance()));
	p.set_mu(r.mu() * p.variance());
}

void performance_to_skill(const Performance& p, Skill& r)
{
	r.set_mu(p.mu() / p.variance());
	r.set_variance(sqrt(1 / p.variance()));
}

Skill new_rating(const trueskill_t& env)
{
	Skill ret;
	ret.set_mu(env.mu);
	ret.set_sigma(env.sigma);
	return ret;
}

double draw_margin(const double& draw_prob, const trueskill_t& env,
		const int& a, const int& b)
{
	return (1 / phi((draw_prob + 1.0) / 2.0)) * sqrt(a + b) * env.beta;
}

matchratings_t calculate_new_ratings(const matchratings_t& teams,
		const RankGroup& results)
{
	// Iterators
	matchratings_t::const_iterator rg_it;
	ratinggroup_t::const_iterator r_it;
	matchprecisions_t::iterator mp_it;
	perfgroup_t::iterator p_it;

	// For all players, convert rating to precision
	matchprecisions_t precisions = matchprecisions_t();
	precisions.resize(teams.size());
	mp_it = precisions.begin();
	for (rg_it = teams.begin(); rg_it != teams.end(); ++rg_it)
	{
		++mp_it;
		mp_it->resize(rg_it->size());
		p_it = mp_it->begin();
		for (r_it = rg_it->begin(); r_it != rg_it->end(); ++r_it)
		{
			skill_to_performance(*r_it, *p_it);
		}
	}

	// For all precisions

}

