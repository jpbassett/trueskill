/*
 * math.cpp
 *
 *  Created on: Mar 6, 2018
 *      Author: jesse
 */

#include "trueskillutil.h"

#include <math.h>

const double a1 = 0.254829592;
const double a2 = -0.284496736;
const double a3 = 1.421413741;
const double a4 = -1.453152027;
const double a5 = 1.061405429;
const double p = 0.3275911;

const double sqrt2 = 1.414213562;

double phi(double x) {
	int sign;
	if (x < 0) {
		sign = -1;
	} else {
		sign = 1;
	}
	double y = fabs(x) / sqrt2;

	// A&S formula 7.1.26
	double t = 1.0 / (1.0 + p * x);
	double z = 1.0
			- (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t
					* exp(-y * y);

	return 0.5 * (1.0 + sign * z);
}
