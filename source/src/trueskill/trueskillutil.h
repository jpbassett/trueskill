/**
 * \file trueskillutil.h
 *
 *  Created on: Mar 6, 2018
 *      Author: jesse
 */

#ifndef INC_TSUTIL_H_
#define INC_TSUTIL_H_

/**
 * \brief Cumulative Distribution Function
 *
 * Taken from https://www.johndcook.com/blog/cpp_phi/
 */
double phi(double x);

#endif /* INC_MATH_H_ */
