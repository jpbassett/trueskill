/*
 * glicko.c
 *
 *  Created on: Jun 21, 2018
 *      Author: jesse
 */

#include "glicko.h"

#include <math.h>

float new_RD(const struct GlickoEnv env, const float RD)
{
	float ret;

	ret = sqrt(RD * RD + env.c * env.c);
	return ret > env.initial_rating.RD ? env.initial_rating.RD : ret;
}

void rate(const struct GlickoEnv env, const struct GlickoOutcome* match,
		struct GlickoOutcome* result)
{
	result->p1 = newRD(env, match->p1.RD);
	result->p2 = newRD(env, match->p2.RD);

}
