/*
 * Gaussian.h
 *
 *  Created on: May 27, 2018
 *      Author: jesse
 */

#ifndef SRC_GAUSSIAN_H_
#define SRC_GAUSSIAN_H_

class Gaussian
{
public:
	Gaussian();
	Gaussian(const double mu, const double variance);
	~Gaussian();

	double mu() const
	{
		return this->m_mu;
	}

	void set_mu(const double mu)
	{
		this->m_mu = mu;
	}

	double variance() const
	{
		return this->m_variance;
	}

	void set_variance(const double variance);

	double sigma() const
	{
		return this->m_sigma;
	}

	void set_sigma(const double sigma);

private:
	double m_mu;
	double m_variance;
	double m_sigma;
};

#endif /* SRC_GAUSSIAN_H_ */
