/**
 * \file elo.h
 *
 *  Created on: May 28, 2018
 *      Author: jesse
 */

#ifndef INC_ELO_H_
#define INC_ELO_H_

#include <vector>

const float LOSS = 0.0;
const float DRAW = 0.5;
const float WIN = 1.0;

/** \brief Type for rating values */
typedef double rating_t;
/** \brief Type for outcomes */
typedef float outcome_t;

struct EloRating
{
	rating_t rating;
	unsigned int experience;
};

struct BatchResult
{
	unsigned int p1_idx;
	unsigned int p2_idx;
	outcome_t result;
};

struct EloEnv
{
	unsigned int provisional_games;
	double provisional_k;
	double experienced_k;
	rating_t initial_rating;
	bool gradiated_k;
};

const struct EloEnv DEFAULT_ENV =
{ 20, 40, 20, 1000, true };

/**
 * \brief Compare two players in the ELO ranking system
 *
 * \param env The ELO environment to use
 * \param p1 The first player
 * \param p2 The second player
 * \param outcome The outcome of the game, where 1.0 is a win for p1, 0.0 is a win for p2, and 0.5 is a draw.
 * \param rslt_p1 A result struct for player 1
 * \param rslt_p2 A result struct for player 2
 */
void rate_1v1(const struct EloEnv& env, const struct EloRating& p1,
		const struct EloRating& p2, const outcome_t outcome,
		struct EloRating& rslt_p1, struct EloRating& rslt_p2);

/**
 * \brief Rate a set of players in a batch.
 *
 * This is used to calculate scores for the results of a tournament between multiple players.
 * Each player may play against any other number of players any other number of times.
 * The scores will be adjusted based on the total performance over the entire game set.
 *
 * \param env The ELO environment
 * \param players A vector of initial ratings
 * \param outcomes A vector of the game outcomes from the tounament, where the player IDs are indexes into the players list.
 * \param results A vector that will contain the results of the ratings.
 */
void rate_batch(const struct EloEnv& env, const std::vector<EloRating>& players,
		const std::vector<BatchResult>& outcomes,
		std::vector<EloRating>& results);

#endif /* INC_ELO_H_ */
