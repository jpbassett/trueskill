/*
 * glicko.h
 *
 *  Created on: Jun 21, 2018
 *      Author: jesse
 */

#ifndef INC_GLICKO_H_
#define INC_GLICKO_H_
#include <vector>


struct GlickoRating
{
	float rating;
	float RD;
};

const float G_WIN = 1.0;
const float G_DRAW = 0.5;
const float G_LOSS = 0.0;

struct GlickoOutcome
{
	struct GlickoRating p1;
	struct GlickoRating p2;
	float outcome;
};

struct GlickoEnv
{
	struct GlickoRating initial_rating;
	float c;
};

void rate(const struct GlickoEnv env, const struct GlickoOutcome* match, struct GlickoOutcome* result);


#endif /* INC_GLICKO_H_ */
