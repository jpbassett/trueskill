/**
 * trueskill.hpp
 *
 *  Created on: Feb 14, 2018
 *      Author: jesse
 */

#ifndef INC_TRUESKILL_HPP_
#define INC_TRUESKILL_HPP_

#include "Gaussian.h"
#include <vector>


class Skill : public Gaussian {

};

class Performance : public Gaussian {

};

//! A list of ratings
typedef std::vector<Skill> ratinggroup_t;

//! Player ratings grouped into teams
typedef std::vector<ratinggroup_t> matchratings_t;

//! Ranks for the player group
typedef std::vector<float> RankGroup;

/**
 * \brief Convert a rating to an adjusted mean
 *
 * \param r The source rating
 * \param p The destination precision
 */
void skill_to_performance(const Skill& r, Performance& p);

/**
 * \brief Convert an adjusted mean to a rating
 *
 * \param p The source precision
 * \param r The destination rating
 */
void performance_to_skill(const Performance& p, Skill& r);

//! The default mean to set the environment to
#define DEFAULT_MU 25

//! The default confidence for the default environment
#define DEFAULT_SIGMA (DEFAULT_MU * DEFAULT_MU / 9)

//! The default 76% win percent rating distance interval
#define DEFAULT_BETA (DEFAULT_SIGMA / 2)

//! The default rating adjustment for each match
#define DEFAULT_TAU (DEFAULT_SIGMA / 100)

//! The default probability that a match will result in a tie
#define DEFAULT_DRAW_PROB 0.0001

typedef struct TrueSkill
{
	double mu;
	double sigma;
	double tau;
	double beta;
	double draw_prob;
} trueskill_t;

const trueskill_t DEFAULT_ENV =
{ DEFAULT_MU, DEFAULT_SIGMA, DEFAULT_TAU,
DEFAULT_BETA, DEFAULT_DRAW_PROB };

/**
 * \brief Create a new rating from the environment
 *
 * \param env The TrueSkill environment
 *
 * \return A rating, synchronized with the environment
 */
Skill new_rating(const trueskill_t& env);

matchratings_t calculate_new_ratings(const matchratings_t& teams,
		const RankGroup& results);

/**
 * \brief Calculate the draw_margin from the draw_probability
 *
 * \param draw_prob The draw probability
 * \param env The trueskill environment
 * \param asize The number of players on the first team
 * \param bsize The number of players on the second team
 *
 * \return The draw margin
 */
double draw_margin(const double& draw_prob, const trueskill_t& env,
		const int& asize, const int& bsize);



#endif /* INC_TRUESKILL_HPP_ */
