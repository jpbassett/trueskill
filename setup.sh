#!/bin/bash

root=$(dirname $(readlink -e $0))
cd ${root}
mkdir -pv {build,eclipse}

buildtype="Unix Makefiles"

cd ${root}/build && cmake -G "$buildtype" ../source
cd ${root}/eclipse && cmake -DCMAKE_ECLIPSE_VERSION="4.7 (Oxygen)" -G "Eclipse CDT4 - $buildtype" ../source
